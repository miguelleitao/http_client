#
# Makefile for http_client
#
# git@gitlab.com:miguelleitao/http_client.git
#

default: http_client.o

all: http_get http_client.o

# To provide DNS name resolutions, include either:
#      -DUSE_GETADDRINFO       or 
#      -DUSE_GETHOSTBYNAME
CFLAGS=-Wall -Wextra -DUSE_GETADDRINFO

http_get: http_client.c http_client.h
	cc ${CFLAGS} -o $@ -DSTANDALONE $<
	strip $@

t_http_get: http_client.c http_client.h
	cc ${CFLAGS} -g -coverage -o $@ -DSTANDALONE $<

http_client.o: http_client.c http_client.h
	cc -c ${CFLAGS} -O2 $<

test: http_get
	./http_get www.google.com / >$@-page.html
	echo -n "Got file size: "
	@stat -c '%s' $@-page.html
	[ `stat -c '%s' $@-page.html` -gt 500 ] 
	[ `stat -c '%s' $@-page.html` -lt 45000 ]

mem-test: t_http_get
	valgrind --leak-check=full --show-leak-kinds=all --suppressions=dlopen.supp ./$< www.google.com / >$@-page0.html

cov-test: t_http_get
	@rm -f http_client.c.gcov
	-./$<
	-./$< -invalid_option nosite.non_existant_dns_domain.net /
	-./$< nosite.non_existant_dns_domain.net /
	-./$< www.google.com /nopage000.html >$@-page1.html
	./$< www.google.com / >$@-page2.html
	./$< -h header=void -o $@-page3.html www.example.com /
	./$< 1.1.1.1 / >$@-page4.html
	./$< -p httpbin.org /anything example_data >$@-page5.html
	./$< -h header=void -o $@-page6.html httpbin.org /anything
	gcov --object-file $<-http_client http_client.c
	
clean:
	rm -f *.o t_http_get http_get 
	rm -f t*-page.html *-page?.html mem-t*.html
	rm -f *.c.gcov *.gcda *.gcno

push:
	git add *.c *.h *.yml Makefile README.* *.supp .gitignore
	git commit -m "update"
	git push
