[![Build status](https://gitlab.com/miguelleitao/http_client/badges/master/pipeline.svg)](https://gitlab.com/miguelleitao/http_client/pipelines)
[![coverage report](https://gitlab.com/miguelleitao/http_client/badges/master/coverage.svg)](https://gitlab.com/miguelleitao/http_client/-/commits/master)

# http_client

Standalone and linkable http client.

Supports GET and POST methods.

https://gitlab.com/miguelleitao/http_client

