/*
 * http_client.h
 *
 */

int MaskCidr(char *mask);
int create_tcp_socket();
char *get_ip(char *host);
char *build_get_query(char *host, char *page, char *);
int http_client_output(char *host, char *page, int size);
int http_client(char *host, char *page, int size, char *outbuf, int bufsize);
int GetIpMaskMac(char *ip, char *mask, char *mac);
int HttpGet(char *host, char *page);
int HttpGetFull(char *host, char *page, char *headers, char *outbuf, int bufsize);
int HttpPost(char *host, char *page, unsigned char *data, int dsize);
int SendHttpQuery(char *host, int port, char *get, int size);

extern char *UserAgent;
