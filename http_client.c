/*
 * HTTP Client
 *
 * jml, 2011, 2016, 2018
 *
 */

#define _GNU_SOURCE 
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <poll.h>
#include <ifaddrs.h>
#include <linux/if.h>
#include <sys/param.h>
#include <fcntl.h>

#include "http_client.h"

#ifdef STANDALONE
void usage();
#endif

#define QUOTE(name) #name
#define STR(macro) QUOTE(macro)
       
#define HTTP_PORT 80
       
#define DEFAULT_PORT HTTP_PORT

#define IP_ADDR_LEN (39)

static int debug = 0;

//	Default UserAgent
//	Can be tunned by user
char *UserAgent = "http_client";

#ifdef DNS_CACHE
  // store cached IP
  char cache_host_ip[IP_ADDR_LEN] = "";
  // Count cached_IP uses, to implement TTL
  int cache_host_ip_uses = 0;

  #ifdef CACHE_HOST
  #define CACHE_HOST_NAME STR(CACHE_HOST)
  const char cache_host_name[] = CACHE_HOST_NAME;
  #else
  char cache_host_name[200] = "";
  #endif
#endif

/*
int GetIpMaskMac_old(char *ip, char *mask, char *mac)
{
	FILE * fp = popen("ifconfig", "r");
        if (fp) {
                char *line=NULL, *e; size_t n=0;
                while ((getline(&line, &n, fp) > 0) && line) {
		   char *p;
                   if ( (p = strstr(line, "inet addr:")) ) {
                        if ( (e = strchr(p+10, ' ')) ) {
                             *e='\0';
                             //printf("  IP:%s\n", p+10);
			     if ( ! strcmp(p+10,"127.0.0.1") ) continue;
                             strcpy(ip,p+10);
			     if ( (p = strstr(e+1,"Mask:")) ) {
				if ( (e = strchr(p+5, '\n')) ) *e='\0';
				strcpy(mask,p+5);
				break;
			     }
                        }
                   }
		   if ( (p = strstr(line, "HWaddr ")) ) {
			if ( (e = strchr(p+7, '\n')) ) *e='\0';
			if ( (e = strchr(p+7, ' ' )) ) *e='\0';
		        strcpy(mac,p+7);
		   }
                }
		if (line) free(line);
		pclose(fp);
        }
        return 0;
}
*/

#ifndef STANDALONE
int getMAC(char *iface, char *mac)
{
  #define MAC_STRING_LENGTH 13
  struct ifreq s;
  int fd = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);

  strcpy(s.ifr_name, iface);
  if (fd >= 0) {
    if ( 0 == ioctl(fd, SIOCGIFHWADDR, &s) ) {
      int i;
      for (i = 0; i < 6; ++i)
        snprintf(mac+i*2,MAC_STRING_LENGTH-i*2,"%02x",(unsigned char) s.ifr_addr.sa_data[i]);
    }
    else  {
      perror("ioctl failed");
      return 0;
    }
    close(fd);
    return 1;
  }
  perror("socket failed");
  return 0;
}

int GetIpMaskMac(char *ip, char *mask, char *mac)
{
	int res = 0;
	struct ifaddrs *addrs, *tmp;
	getifaddrs(&addrs);
	tmp = addrs;

	while (tmp) 
	{
	    if (tmp->ifa_addr && tmp->ifa_addr->sa_family == AF_INET)
	    {
		    if ( strcmp(tmp->ifa_name,"lo")!=0  ) {
			    struct sockaddr_in *pAddr = (struct sockaddr_in *)tmp->ifa_addr;
			    strcpy(ip, inet_ntoa(pAddr->sin_addr) );
			    pAddr = (struct sockaddr_in *)tmp->ifa_netmask;
			    strcpy(mask, inet_ntoa(pAddr->sin_addr) );
			    getMAC(tmp->ifa_name,mac);
			    //printf("%s: %s\n", tmp->ifa_name, inet_ntoa(pAddr->sin_addr));
			    res = 1;
			    break;		// One interface addr got. Getout.
		    }
	    }
	    tmp = tmp->ifa_next;
	}
	freeifaddrs(addrs);
	return res;
}

int MaskCidr(char *mask)
{
 int i, v;
 int total=0;
 char *b = mask, *e;
 for( i=0 ; i<4 ; i++ ) {
   if ( (e = strchr(b,'.')) ) *e='\0';
      v = atoi(b);
      switch (v) {
            case 0b00000000: 	return total;
            case 0b10000000:	return total+1;
            case 0b11000000:	return total+2;
            case 0b11100000:    return total+3;
            case 0b11110000:    return total+4;
            case 0b11111000:    return total+5;
            case 0b11111100:    return total+6;
            case 0b11111110:    return total+7;
            case 0b11111111:    total+=8;
      }
      if ( !e ) return total;
      b = e+1;
 }
 return total; 
}
#endif

#ifdef STANDALONE  
int main(int argc, char **argv)
{
  #ifdef DNS_CACHE
  #ifdef CACHE_HOST
    if ( cache_host_name && *cache_host_name )
        fprintf(stderr,"DNS caching enabled for host '%s': %s\n", cache_host_name, cache_host_ip);
  #endif
  #endif
  if (argc<2) {
    usage(argv[0]);
    //fprintf(stderr,"Usage %s host page\n",argv[0]);
    exit( 1 );
  }
  char *headers = NULL;
  short int post = 0;
  char *outbuf = NULL;
  int fout = -1;
  while( argc>2 && *argv[1]=='-' ) {
      switch (argv[1][1]) {
        case 'h':
            if ( argv[3] && *argv[3] ) {
                headers = argv[3];
                argc--; argv++;
            }
            break;
        case 'p':
            post = 1;
            break;
        case 'o':
            mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
            fout = open(argv[2], O_WRONLY | O_CREAT | O_TRUNC, mode);
            argc--; argv++;
            break;
        default:
            fprintf(stderr, "Invalid option: %s\n", argv[1]);
      }
      argc--; argv++;
  }
  int res = -1;
  if ( post ) 
      res = HttpPost(argv[1], argv[2], (unsigned char *)argv[3], strlen(argv[3]));
  else {
      if ( headers ) res = HttpGetFull(argv[1], argv[2], headers, outbuf, fout);
      else res = HttpGet(argv[1],argv[2]);
  }
  if ( ! outbuf && fout>=0 ) {
      close(fout);
  }
  return res;
}
#endif
 
int SendHttpQuery(char *host, int port, char *get, int size)
{
    struct sockaddr_in *remote;
    int sock;
    char *ip;
    //char *get;
    int tmpres;

	if ( port<0 ) port = DEFAULT_PORT;

        sock = create_tcp_socket();
        ip = get_ip(host);
        if ( ip ) {
		if ( *ip ) {
		  //fprintf(stderr, "IP is %s\n", ip);
		  remote = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in));
		  remote->sin_family = AF_INET;
		  tmpres = inet_pton(AF_INET, ip, (void *)(&(remote->sin_addr.s_addr)));
		  if ( tmpres < 0)  {
		    perror("Can't set remote->sin_addr.s_addr");
		  } 
		  else {
		    if(tmpres == 0) 
		        fprintf(stderr, "%s is not a valid IP address\n", ip);
		    else {
		        remote->sin_port = htons(port);

		        if (connect(sock, (struct sockaddr *)remote, sizeof(struct sockaddr)) < 0) {
		            perror("Could not connect");
		        } 
			    else {
			        //Send the query to the server
			        int sent = 0;
			        tmpres = 0;
		            while ( sent<size && tmpres>=0 ) {
		                tmpres = send(sock, get+sent, size-sent, 0);
		                if (tmpres == -1)
		                    perror("Can't send query");
		                sent += tmpres;
		            }
			    }
		    }
		  }
		  free(remote);
		}
        free(ip);
    }
	return sock;
}

	
int http_client(char *host, char *page, int size, char *outbuf, int bufsize)
{
	//	page can point to binary data.
	//	size is the dimension of page data.
	//	outbuf must be allocated by the caller. 
	//	if outbuf==NULL, bufsize if used as an handle for an opened output file.
	//	bufsize is the space alocated for outbuf or the outfile handle.
	//	if outbuf==NULL && buzsize<0, the output is sent to stdout.
    int sock;
    int tmpres;
	int total_size = 0;
    char buf[BUFSIZ+1];	// line buffer
	//printf("http_client %s %s\n", host,page);   
	sock = SendHttpQuery(host, HTTP_PORT, page, size);
	if ( sock>=0 ) {
	  //printf("query sent\n");
          //now it is time to receive the page
          memset(buf, 0, sizeof(buf));
          int htmlstart = 0;
          char * htmlcontent;
	  
	  struct pollfd fds[1];
	  fds[0].fd = sock;
	  fds[0].events = POLLIN | POLLPRI;
          while(1) {
	    tmpres = poll(fds, 1, 4000);
	    if ( tmpres==-1 ) {
    		perror("poll"); // error occurred in poll()
	    } else if (tmpres == 0) {
    		printf("Timeout occurred!  No data after 3.5 seconds.\n");
		return 0;
	    }

	    tmpres = recv(sock, buf, BUFSIZ, MSG_DONTWAIT);
	    if ( tmpres==0 ) break;
	    if ( tmpres<0 ) {
			if (errno == EAGAIN) continue;
			fprintf(stderr,"Reading error\n");
			break;
	    }
            if (htmlstart == 0) {
               /* Under certain conditions this will not work.
                * If the \r\n\r\n part is splitted into two messages
                * it will fail to detect the beginning of HTML content
                */
               htmlcontent = strstr(buf, "\r\n\r\n");
               if (htmlcontent != NULL) {
    		  htmlstart = 1;
                  htmlcontent += 4;
               }
            } else htmlcontent = buf;
	    
	    if (htmlstart) {
		int data_size = tmpres - ( buf-htmlcontent );
		if ( outbuf ) {
			int out_data_size = MIN(data_size,bufsize-total_size);
			memcpy(outbuf, htmlcontent, out_data_size );
			total_size += out_data_size;
		} 
		else {
			if ( bufsize >= 0 ) { 	// buf_size is a handle for an opened file
				int res = write(bufsize,htmlcontent,data_size);
				if ( res<0 ) fprintf(stderr,"Write failed: %s\n",strerror(errno));
			}
			else			// output to stdout
				puts(htmlcontent);
			total_size += data_size;
		}
	    }
            memset(buf, 0, tmpres);
          } // while
          if(tmpres < 0) 	perror("Error receiving data");
          close(sock);
	  //printf("page got\n");
	  if ( htmlstart && strcmp(htmlcontent,"OK\n")==0 )	return 1;
	}
        return 0;
}

#ifdef STANDALONE
void usage(char *app_name)
{
    fprintf(stderr, "USAGE: %s host [page]\n", app_name);
    fprintf(stderr, "          \thost: the website hostname. ex: coding.debuntu.org\n");
	fprintf(stderr, "          \tpage: the page to retrieve. ex: index.html, default: /\n");
}
#endif

int create_tcp_socket()
{
   int sock;
   if((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0){
       perror("Can't create TCP socket");
   }

   /*************************************************************/
   /* Set socket to be non-blocking.                            */
   /*************************************************************/
/*
   res = ioctl(sock, FIONBIO, (char *)&on);
   if (res < 0)
   {
      perror("ioctl() failed");
      close(sock);
      return 0;
   }
*/
   return sock;
}

char *get_ip(char *host)
{
	int iplen = IP_ADDR_LEN; //XXX.XXX.XXX.XXX
    char *ip = (char *)malloc(iplen+1);

    if ( *host<='9' && *host>='0' && strlen(host)<=15) {
	    // host is an IPv4 address.
        strncpy(ip, host, iplen);
        return ip;
    }

    #ifdef DNS_CACHE
	    // Single entry cache table.
        if (strcmp(host, cache_host_name)==0) {
            if ( *cache_host_ip && cache_host_ip_uses<20 ) {
                // Use cached IP
                strcpy(ip, cache_host_ip);
                cache_host_ip_uses++;
                return ip;
            }
        }
    #endif

	// Get first IPv4 addr for host.
	memset(ip, 0, iplen+1);

	#ifdef USE_GETADDRINFO
	    // Uses getaddrinfo().
	    // getaddrinfo() can be used for IPv4 and IPv6.
	    // Returns a pointer to a new allocated string with the obtained addr. 
            //struct hostent *hent;
            struct addrinfo hints, *addrs;
            memset (&hints, 0, sizeof(hints));
            hints.ai_family = AF_INET; 	// Use AF_UNSPEC instead to allow getaddrinfo()
                               		// to find both IPv4 and IPv6 addresses for the hostname.
                               		// Just make sure the rest of your code is equally family-
                               		// agnostic when dealing with the IP addresses associated
                               		// with this connection. For instance, make sure any uses
                               		// of sockaddr_in are changed to sockaddr_storage,
                               		// and pay attention to its ss_family field, etc...
    	    hints.ai_socktype = SOCK_STREAM;
    	    hints.ai_protocol = IPPROTO_TCP;
    	
            //printf("calling getaddrinfo()\n");
            int err = getaddrinfo(host, NULL, &hints, &addrs);
    	    if (err != 0) {
                fprintf(stderr, "Error: %s: %s\n", host, gai_strerror(err));
                exit(1);
            }
        
            if (inet_ntop(AF_INET, &((struct sockaddr_in *) addrs->ai_addr)->sin_addr, ip, iplen) == NULL) {
                perror("Can't resolve host");
            }
            free(addrs);
	#else
	    #ifdef USE_GETHOSTBYNAME
	        // gethostbyname is deprecated.
                if ((hent = gethostbyname(host)) == NULL) {
                    char msg[120];
                    sprintf(msg,"Can't get IP address for host '%s'", host);
                    herror(msg);
                }
                else {
                    if (inet_ntop(AF_INET, (void *)hent->h_addr_list[0], ip, iplen) == NULL) {
                        perror("Can't resolve host");
                    }
                    // Valgrind says invalid free here.
                    // gethostbyname can return poiter poiter to static data.
                    //free(hent);
                }
        #endif
    #endif

	if ( *ip==0 ) 
	    fprintf(stderr, "Can't resolve host '%s'\n", host);

	#ifdef DNS_CACHE
	// Store in cache.
	if (strcmp(host,cache_host_name)!=0) {
	    cache_host_ip_uses = 0;
	    #ifndef CHACHE_HOST
	        strcpy(cache_host_name, host);
	    #endif
	    strcpy(cache_host_ip, ip);
	}
	#endif

    return ip;
}
/*
char *get_ip(char *host)
{
	// Get first IPv4 addr for host.
	// Uses deprecated gethostbyname() and a local, single entry, cache.
	// Returns a pointer to a new allocated string with the obtained addr. 
        struct hostent *hent;
        int iplen = 15; //XXX.XXX.XXX.XXX
        char *ip = (char *)malloc(iplen+1);

	#ifdef CACHE_HOST
	if (strcmp(host,cache_host_name)==0) {
	    if ( *cache_host_ip && cache_host_ip_uses<20 ) {
		// Use cached IP
	    	strcpy(ip, cache_host_ip);
		cache_host_ip_uses++;
	    	return ip;
	    }
	}
	#endif

	if ( *host<='9' && *host>='0' ) {
	    strncpy(ip,host,iplen);
	    return ip;
	}
        memset(ip, 0, iplen+1);


        if ((hent = gethostbyname(host)) == NULL)
        {
	    char msg[120];
	    sprintf(msg,"Can't get IP address for host '%s'", host);
            herror(msg);
        }
	else {
          if (inet_ntop(AF_INET, (void *)hent->h_addr_list[0], ip, iplen) == NULL)
          {
            perror("Can't resolve host");
          }
	  // Valgrind says invalid free here.
	  // gethostbyname can return poiter poiter to static data.
	  //free(hent);
	}

	#ifdef CACHE_HOST
	if (strcmp(host,cache_host_name)==0) {
	    cache_host_ip_uses = 0;
	    strcpy(cache_host_ip, ip);
	}
	#endif

        return ip;
}
*/ 

char *build_get_query(char *host, char *page, char *headers)
{
    char *query;
    char *getpage = page;
    char *tpl = "GET /%s HTTP/1.0\r\nHost: %s\r\nUser-Agent: %s\r\n%s\r\n";
    if(getpage[0] == '/'){
      getpage = getpage + 1;
      //fprintf(stderr,"Removing leading \"/\", converting %s to %s\n", page, getpage);
    }
    int headers_len = 0;
    char headers_fix[1] = "";
    if ( headers ) 
	    headers_len = strlen(headers);
	else
	    headers = headers_fix;    
        
    query = (char *)malloc(strlen(host)+strlen(getpage)+strlen(UserAgent)+strlen(tpl) + headers_len );
    sprintf(query, tpl, getpage, host, UserAgent, headers );
    return query;
}
/*
int HttpGet(char *host, char *page)
{
    char *page_c = page;
    char default_page[] = "/";
    if ( page==NULL ) page_c = default_page;
    char *query = build_get_query(host, page_c, NULL);
	if ( debug>90 ) printf("Query build: '%s'\n", query);
    int res = http_client(host, query, strlen(query), NULL, -1);
    free(query);
    return res;
}
*/


int HttpGet(char *host, char *page)
{
    return HttpGetFull(host, page, NULL, NULL, -1);;
}

int HttpGetFull(char *host, char *page, char *headers, char *outbuf, int bufsize)
{
    char *page_c = page;
    char default_page[] = "/";
    if ( page==NULL ) page_c = default_page;
    char *query = build_get_query(host, page_c, headers);
	if ( debug>90 ) printf("Query build: '%s'\n", query);
    int res = http_client(host, query, strlen(query), outbuf, bufsize);
    free(query);
    return res;
}

/*
char *build_post_query(char *host, char *page, int size, unsigned char *data)
{
    char *query;
    char *getpage = page;
    char *tpl = "POST /%s HTTP/1.0\r\nHost: %s\r\nUser-Agent: %s\r\nContent-Length: %d\r\n\r\n";
    if(getpage[0] == '/'){
      getpage = getpage + 1;
      //fprintf(stderr,"Removing leading \"/\", converting %s to %s\n", page, getpage);
    }
    query = (char *)malloc(strlen(host)+strlen(getpage)+strlen(UserAgent)+strlen(tpl)+15+size );
    sprintf(query, tpl, getpage, host, UserAgent, size );
	memcpy(query+strlen(query), data, size);
	
    return query;
}
*/

int HttpPost(char *host, char *page, unsigned char *data, int size)
{
    char *query;
    char *getpage = page;
    char *tpl = "POST /%s HTTP/1.0\r\nHost: %s\r\nUser-Agent: %s\r\nContent-Length: %d\r\n\r\n";
    if(getpage[0] == '/'){
      getpage = getpage + 1;
      //fprintf(stderr,"Removing leading \"/\", converting %s to %s\n", page, getpage);
    }
    query = (char *)malloc(strlen(host)+strlen(getpage)+strlen(UserAgent)+strlen(tpl)+15+size );
    sprintf(query, tpl, getpage, host, UserAgent, size );
    int total_size = size+strlen(query);
    memcpy(query+strlen(query), data, size);

    int res = http_client(host, query, total_size, NULL, -1);
    free(query);
    return res;
}

